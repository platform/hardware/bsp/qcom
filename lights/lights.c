/*
 * Copyright (C) 2008 The Android Open Source Project
 * Copyright (C) 2014 - 2016 The  Linux Foundation. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


// #define LOG_NDEBUG 0

#include <cutils/log.h>

#include <string.h>
#include <errno.h>
#include <pthread.h>
#include <stdlib.h>

#include <hardware/lights.h>

/******************************************************************************/
#define SET_LIGHT(color)    (color & 0xffffff ? 0x28 : 0)
#define TRIG_MODE_NONE      "none"
#define TRIG_MODE_TIMER     "timer"

typedef enum {
    HAL_LIGHT_ID_LED1 = 0,
    HAL_LIGHT_ID_LED2,
    HAL_LIGHT_ID_LED3,
    HAL_LIGHT_ID_BT,
    HAL_LIGHT_ID_WIFI,
    HAL_LIGHT_ID_MAX
} light_hal_device_id;

typedef struct {
    char const * const brightness;
    char const * const delay_on;
    char const * const delay_off;
    char const * const trigger;
} light_device_sysfs_nodes;

static light_device_sysfs_nodes dev_nodes[] =
{
    {
        "/sys/class/leds/led1/brightness", "/sys/class/leds/led1/delay_on",
        "/sys/class/leds/led1/delay_off", "/sys/class/leds/led1/trigger"
    },

    {
        "/sys/class/leds/led2/brightness", "/sys/class/leds/led2/delay_on",
        "/sys/class/leds/led2/delay_off", "/sys/class/leds/led2/trigger"
    },

    {
        "/sys/class/leds/led3/brightness", "/sys/class/leds/led3/delay_on",
        "/sys/class/leds/led3/delay_off", "/sys/class/leds/led3/trigger"
    },

    {
        "/sys/class/leds/bt/brightness", "/sys/class/leds/bt/delay_on",
        "/sys/class/leds/bt/delay_off", "/sys/class/leds/bt/trigger"
    },

    {
        "/sys/class/leds/wlan/brightness", "/sys/class/leds/wlan/delay_on",
        "/sys/class/leds/wlan/delay_off", "/sys/class/leds/wlan/trigger"
    },
};

static pthread_once_t g_init = PTHREAD_ONCE_INIT;
static pthread_mutex_t g_lock = PTHREAD_MUTEX_INITIALIZER;

/**
 * device methods
 */
void init_globals(void)
{
    // init the mutex
    pthread_mutex_init(&g_lock, NULL);
}

static int write_int(char const* path, int value)
{
    FILE *fd;

    fd = fopen(path, "w+");
    if (fd) {
        int bytes = fprintf(fd, "%d", value);
        fclose(fd);
        return (bytes < 0 ? bytes : 0);
    } else {
        ALOGE("write_int failed to open %s\n", path);
        return -errno;
    }
}

static int write_str(char const* path, char const* str)
{
    FILE *fd;

    fd = fopen(path, "w+");
    if (fd) {
        int bytes = fprintf(fd, "%s", str);
        fclose(fd);
        return (bytes < 0 ? bytes : 0);
    } else {
        ALOGE("write_int failed to open %s\n", path);
        return -errno;
    }
}

static int disable_timed_blink(light_hal_device_id dev_id)
{
    int err = 0;

    if (dev_id >= HAL_LIGHT_ID_MAX)
        return -EINVAL;

    pthread_mutex_lock(&g_lock);
    err = write_str(dev_nodes[dev_id].trigger, TRIG_MODE_NONE);
    pthread_mutex_unlock(&g_lock);
    return err;
}

static int enable_timed_blink(light_hal_device_id dev_id,
                              struct light_state_t const* state)
{
    int err = 0;

    if (dev_id >= HAL_LIGHT_ID_MAX)
        return -EINVAL;

    pthread_mutex_lock(&g_lock);
    if((err = write_str(dev_nodes[dev_id].trigger, TRIG_MODE_TIMER)))
        goto exit;
    if((err = write_int(dev_nodes[dev_id].delay_on, state->flashOnMS)))
        goto exit;
    err = write_int(dev_nodes[dev_id].delay_off, state->flashOffMS);

exit:
    pthread_mutex_unlock(&g_lock);
    return err;
}

static int set_led(light_hal_device_id dev_id,
                   struct light_state_t const* state)
{
    int err = 0;

    if (dev_id >= HAL_LIGHT_ID_MAX)
        return -EINVAL;

    pthread_mutex_lock(&g_lock);
    err = write_int(dev_nodes[dev_id].brightness, SET_LIGHT(state->color));
    pthread_mutex_unlock(&g_lock);

    return err;
}

static int set_light_notifications(struct light_device_t* dev,
        struct light_state_t const* state)
{
    int err = 0;

    if(!dev)
        return -1;

    switch (state->flashMode) {
        case LIGHT_FLASH_TIMED:
            err = enable_timed_blink(HAL_LIGHT_ID_LED1, state);
            break;

        case LIGHT_FLASH_NONE:
            err = disable_timed_blink(HAL_LIGHT_ID_LED1);
            set_led(HAL_LIGHT_ID_LED1, state);
            break;

        default:
            err = -EINVAL;
    }

    return err;
}

static int set_light_attention(struct light_device_t* dev,
        struct light_state_t const* state)
{
    int err = 0;

    if(!dev)
        return -1;

    switch (state->flashMode) {
        case LIGHT_FLASH_TIMED:
            err = enable_timed_blink(HAL_LIGHT_ID_LED2, state);
            break;

        case LIGHT_FLASH_NONE:
            err = disable_timed_blink(HAL_LIGHT_ID_LED2);
            set_led(HAL_LIGHT_ID_LED2, state);
            break;

        default:
            err = -EINVAL;
    }

    return err;
}

static int set_light_buttons(struct light_device_t* dev,
        struct light_state_t const* state)
{
    int err = 0;
    if(!dev) {
        return -1;
    }

    switch (state->flashMode) {
        case LIGHT_FLASH_TIMED:
            err = enable_timed_blink(HAL_LIGHT_ID_LED3, state);
            break;

        case LIGHT_FLASH_NONE:
            err = disable_timed_blink(HAL_LIGHT_ID_LED3);
            set_led(HAL_LIGHT_ID_LED3, state);
            break;

        default:
            err = -EINVAL;
    }

    return err;
}

static int set_light_bluetooth(struct light_device_t* dev,
        struct light_state_t const* state)
{
    int err = 0;
    if(!dev) {
        return -1;
    }

    switch (state->flashMode) {
        case LIGHT_FLASH_TIMED:
            err = enable_timed_blink(HAL_LIGHT_ID_BT, state);
            break;

        case LIGHT_FLASH_NONE:
            err = disable_timed_blink(HAL_LIGHT_ID_BT);
            set_led(HAL_LIGHT_ID_BT, state);
            break;

        default:
            err = -EINVAL;
    }

    return err;
}

static int set_light_wifi(struct light_device_t* dev,
        struct light_state_t const* state)
{
    int err = 0;
    if(!dev) {
        return -1;
    }

    switch (state->flashMode) {
        case LIGHT_FLASH_TIMED:
            err = enable_timed_blink(HAL_LIGHT_ID_WIFI, state);
            break;

        case LIGHT_FLASH_NONE:
            err = disable_timed_blink(HAL_LIGHT_ID_WIFI);
            set_led(HAL_LIGHT_ID_WIFI, state);
            break;

        default:
            err = -EINVAL;
    }

    return err;
}

/** Close the lights device */
static int close_lights(struct light_device_t *dev)
{
    if (dev) {
        free(dev);
    }
    return 0;
}


/******************************************************************************/

/**
 * module methods
 */

/** Open a new instance of a lights device using name */
static int open_lights(const struct hw_module_t* module, char const* name,
        struct hw_device_t** device)
{
    int (*set_light)(struct light_device_t* dev,
            struct light_state_t const* state);

    if (0 == strcmp(LIGHT_ID_NOTIFICATIONS, name))
        set_light = set_light_notifications;
    else if (0 == strcmp(LIGHT_ID_ATTENTION, name))
        set_light = set_light_attention;
    else if (0 == strcmp(LIGHT_ID_BUTTONS, name))
        set_light = set_light_buttons;
    else if (0 == strcmp(LIGHT_ID_BLUETOOTH, name))
        set_light = set_light_bluetooth;
    else if (0 == strcmp(LIGHT_ID_WIFI, name))
        set_light = set_light_wifi;
    else
        return -EINVAL;

    pthread_once(&g_init, init_globals);

    struct light_device_t *dev = calloc( 1, sizeof(struct light_device_t));

    if(!dev)
        return -ENOMEM;

    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 0;
    dev->common.module = (struct hw_module_t*)module;
    dev->common.close = (int (*)(struct hw_device_t*))close_lights;
    dev->set_light = set_light;

    *device = (struct hw_device_t*)dev;
    return 0;
}

static struct hw_module_methods_t lights_module_methods = {
    .open =  open_lights,
};

/*
 * The lights Module
 */
struct hw_module_t HAL_MODULE_INFO_SYM = {
    .tag = HARDWARE_MODULE_TAG,
    .version_major = 1,
    .version_minor = 0,
    .id = LIGHTS_HARDWARE_MODULE_ID,
    .name = "Lights Module",
    .author = "Codeaurora Author",
    .methods = &lights_module_methods,
};
